//2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
const givenArray = require("./index.js")

let res2 = givenArray.map((Array) => {
    let newArray = (Array["ip_address"].split("."))
    let output = newArray.map((Array) => {
        return Number(Array)

    });
    return output
});
console.log(res2)
module.exports = res2;